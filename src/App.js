import {BrowserRouter, Switch, Route} from "react-router-dom";
import Navigation from "./Components/Navigation/Navigation";
import 'bootstrap/dist/css/bootstrap.min.css';
import ShoppingList from "./Components/ShoppinList/ShoppingList";
import './Style/Cursor.css'
import ShoppingListItems from "./Components/ShoppingListItems/ShoppingListItems";

function App() {
  return (
    <BrowserRouter>
      <Navigation/>

      <Switch>
        <Route exact path="/">
         Kevin alone at Home
        </Route>

        <Route exact path="/shopping-lists">
          <ShoppingList/>
        </Route>

        <Route exact path="/shopping-list/:id">
          <ShoppingListItems/>
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
