import React, {useState} from 'react';

const Counter = (props) => {
  const [count, setCount] = useState(0)

  const incrementCount = () => {
    if (count === 0) {
      setCount(1)
      return
    }
    setCount(count * 2)
  }

  const resetCount = () => {
    setCount(0)
  }

  return (
    <>
      <p>You clicked {count} times</p>
      <button onClick={incrementCount}>HERE</button>
      <button onClick={resetCount}>Reset</button>
    </>
  )
}

export default Counter;
