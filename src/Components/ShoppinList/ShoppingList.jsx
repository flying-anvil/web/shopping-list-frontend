import React from 'react';
import {Card, Col, Row} from "react-bootstrap";
import {useHistory} from "react-router-dom";


const data = {
  "shoppingLists": [
    {
      "id": 1,
      "name": "elektronik",
      "entryCount": 2,
      "createdDate": "2021-05-01T15:31:02+00:00"
    },
    {
      "id": 2,
      "name": "nudeln",
      "entryCount": 1,
      "createdDate": "2021-05-01T14:00:33+00:00"
    },
    {
      "id": 3,
      "name": "newList",
      "entryCount": 2,
      "createdDate": "2021-05-13T16:10:36+00:00"
    },
    {
      "id": 7,
      "name": "Grillen",
      "entryCount": 1,
      "createdDate": "2021-05-13T16:25:03+00:00"
    }
  ]
}


const ShoppingList = (props) => {
  const history = useHistory();

  return (
    <>
      <Col md={12}>
        <Row>
          {data.shoppingLists.map((list) => {
            const date = new Date(list.createdDate);
            return (
              <Col md={6} key={list.id}>
                <Card className={'mb-3 cursor-pointer'}
                      onClick={() => history.push(`/shopping-list/${list.id}`)}>
                  <Card.Header className={'pb-0'}>
                    <Card.Title as={'h3'}>{list.name}</Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <Card.Text>{list.entryCount} Einträge</Card.Text>
                    <Card.Text>Erstelldatum: {date.toLocaleDateString()} {date.toLocaleTimeString()}
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            )
          })}
        </Row>
      </Col>
    </>
  );
}

export default ShoppingList;
