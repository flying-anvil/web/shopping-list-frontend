import React from 'react';
import {useParams} from "react-router-dom";
import {Card, Col, Row} from "react-bootstrap";

const data = {
  'name': 'Elektronik',
  "items": [
    {
      "id": 1,
      "name": "Kabel",
      "amount": 4113,
      "unit": "stk",
      "description": null,
      "added_date": "2021-05-02T15:02:11+00:00"
    },
    {
      "id": 2,
      "name": "diskette",
      "amount": 4113,
      "unit": "Liter",
      "description": "ist alt",
      "added_date": "2021-05-02T15:02:11+00:00"
    }
  ]
}

const ShoppingListItems = (props) => {
  const params = useParams()
  const {id} = params

  return (
    <Col md={12}>
      <Card>
        <Card.Header className={'text-center pb-0'}>
          <Card.Title>
            {data.name}
          </Card.Title>
        </Card.Header>
        <Card.Body>
          {data.items.map((item) => {
            return (
              <Card key={item.id} className={'mb-2'}>
                <Card.Body>
                  <Row>
                    <Col md={1} className={'text-center'}>
                      YAY
                    </Col>
                    <Col>
                      <Row>
                        <Card.Title> {item.name} </Card.Title>
                      </Row>
                      <Row>
                        <Card.Text> {item.amount} {item.unit} </Card.Text>
                      </Row>
                    </Col>
                    <Col>
                      <Row>
                        <Card.Text className={'text-muted'}> {item.description} </Card.Text>
                      </Row>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            )
          })}
        </Card.Body>
      </Card>
    </Col>
  )
    ;
}

export default ShoppingListItems;
